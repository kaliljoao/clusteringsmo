﻿using System;
using System.Collections.Generic;
using Microsoft.ML.Data;
using System.Text;

namespace Clustering
{
    public class ClusterData
    {
        [LoadColumn(0)] public float GIRO_ID { get; set; }
        [LoadColumn(1)] public float PORTA_COM_GIRO { get; set; }
        [LoadColumn(2)] public float IND_SEQ_GIRO { get; set; }
        [LoadColumn(2)] public float ANGULO { get; set; }
        [LoadColumn(3)] public float REFERENCIA { get; set; }
        [LoadColumn(4)] public float DATA_GIRO { get; set; }
        [LoadColumn(5)] public float TS_UNIX_GIRO { get; set; }
        [LoadColumn(6)] public float GPS_ID { get; set; }
        [LoadColumn(7)] public float PORTA_COM_GPS { get; set; }
        [LoadColumn(8)] public float IS_CLEAN { get; set; }
        [LoadColumn(9)] public float IS_DIRTY { get; set; }
        [LoadColumn(10)] public float IND_SEQ_GPS { get; set; }
        [LoadColumn(11)] public float LATITUDE { get; set; }
        [LoadColumn(12)] public float LAT_COORD { get; set; }
        [LoadColumn(13)] public float LONGITUDE { get; set; }
        [LoadColumn(14)] public float LONG_COORD { get; set; }
        [LoadColumn(15)] public float GPS_QUAL { get; set; }
        [LoadColumn(16)] public float NUM_SAT { get; set; }
        [LoadColumn(17)] public float HDOP { get; set; }
        [LoadColumn(18)] public float ALTITUDE { get; set; }
        [LoadColumn(19)] public float GEO_SEPARATION { get; set; }
        [LoadColumn(20)] public float STN { get; set; }
        [LoadColumn(21)] public float AGE { get; set; }
        [LoadColumn(22)] public float UTC { get; set; }
        [LoadColumn(23)] public float DATA_GPS { get; set; }
        [LoadColumn(24)] public float TS_UNIX_GPS { get; set; }
        [LoadColumn(25)] [ColumnName("GPS_ID.1")] public float GPS_ID_1{ get; set; }
        [LoadColumn(26)] [ColumnName("PORTA_COM_GPS.1")] public float PORTA_COM_GPS_1{ get; set; }
        [LoadColumn(27)] [ColumnName("IS_CLEAN.1")] public float IS_CLEAN_1{ get; set; }
        [LoadColumn(28)] [ColumnName("IS_DIRTY.1")] public float IS_DIRTY_1{ get; set; }
        [LoadColumn(29)] [ColumnName("IND_SEQ_GPS.1")] public float IND_SEQ_GPS_1{ get; set; }
        [LoadColumn(30)] [ColumnName("LATITUDE.1")] public float LATITUDE_1{ get; set; }
        [LoadColumn(31)] [ColumnName("LAT_COORD.1")] public float LAT_COORD_1{get ; set;}
        [LoadColumn(32)] [ColumnName("LONGITUDE.1")] public float LONGITUDE_1{get ; set;}
        [LoadColumn(33)] [ColumnName("LONG_COORD.1")] public float LONG_COORD_1{get ; set;}
        [LoadColumn(34)] [ColumnName("GPS_QUAL.1")] public float GPS_QUAL_1{get ; set;}
        [LoadColumn(35)] [ColumnName("NUM_SAT.1")] public float NUM_SAT_1{get ; set;}
        [LoadColumn(36)] [ColumnName("HDOP.1")] public float HDOP_1{get ; set;}
        [LoadColumn(37)] [ColumnName("ALTITUDE.1")] public float ALTITUDE_1{get ; set;}
        [LoadColumn(38)] [ColumnName("GEO_SEPARATION.1")] public float GEO_SEPARATION_1{get ; set;}
        [LoadColumn(39)] [ColumnName("STN.1")] public float STN_1{get ; set;}
        [LoadColumn(40)] [ColumnName("AGE.1")] public float AGE_1{get ; set;}
        [LoadColumn(41)] [ColumnName("UTC.1")] public float UTC_1{get ; set;}
        [LoadColumn(42)] [ColumnName("DATA_GPS.1")] public float DATA_GPS_1{get ; set;}
        [LoadColumn(43)] [ColumnName("TS_UNIX_GPS.1")] public float TS_UNIX_GPS_1{get ; set;}
        [LoadColumn(44)] public float MRU_ID{get ; set;} 
        [LoadColumn(45)] public float PORTA_COM_MRU{get ; set;}
        [LoadColumn(46)] public float IND_SEQ_MRU{get ; set;}
        [LoadColumn(47)] public float ROLL{get ; set;}
        [LoadColumn(48)] public float YAW{get ; set;}
        [LoadColumn(49)] public float PITCH{get ; set;}
        [LoadColumn(50)] public float HEADING{get ; set;}
        [LoadColumn(51)] public float SURGE{get ; set;}
        [LoadColumn(52)] public float SWAY{get ; set;}
        [LoadColumn(53)] public float HEAVE{get ; set;}
        [LoadColumn(54)] public float VELSURGE{get ; set;}
        [LoadColumn(55)] public float VELSWAY{get ; set;}
        [LoadColumn(56)] public float VELHEAVE{get ; set;}
        [LoadColumn(57)] public float ACESURGE{get ; set;}
        [LoadColumn(58)] public float ACESWAY{get ; set;}
        [LoadColumn(59)] public float ACEHEAVE{get ; set;}
        [LoadColumn(60)] public float DATA_MRU{get ; set;}
        [LoadColumn(61)] public float TS_UNIX_MRU{get ; set;}

    }

}
