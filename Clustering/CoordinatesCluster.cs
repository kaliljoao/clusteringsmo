﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clustering
{
    public class CoordinatesCluster
    {
        [ColumnName("Latitudade")]
        public float Latitude { get; set; }
        [ColumnName("Longitude")]
        public float Longitude { get; set; }
    }
}
