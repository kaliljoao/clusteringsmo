﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clustering
{
    public class Observation
    {
        [VectorType(4)]
        public float[] Features;

        public static Observation Create(ClusterData data)
        {
            return new Observation
            {
                Features = new[]
                {
                    (float)data.GIRO_ID,
                    (float)data.PORTA_COM_GIRO,
                    (float)data.IND_SEQ_GIRO,
                    (float)data.ANGULO,
                    (float)data.REFERENCIA,
                    (float)data.DATA_GIRO,
                    (float)data.TS_UNIX_GIRO,
                    (float)data.GPS_ID,
                    (float)data.PORTA_COM_GPS,
                    (float)data.IS_CLEAN,
                    (float)data.IS_DIRTY,
                    (float)data.IND_SEQ_GPS,
                    (float)data.LATITUDE,
                    (float)data.LAT_COORD,
                    (float)data.LONGITUDE,
                    (float)data.LONG_COORD,
                    (float)data.GPS_QUAL,
                    (float)data.NUM_SAT,
                    (float)data.HDOP,
                    (float)data.ALTITUDE,
                    (float)data.GEO_SEPARATION,
                    (float)data.STN,
                    (float)data.AGE,
                    (float)data.UTC,
                    (float)data.DATA_GPS,
                    (float)data.TS_UNIX_GPS,
                    (float)data.GPS_ID_1,
                    (float)data.PORTA_COM_GPS_1,
                    (float)data.IS_CLEAN_1,
                    (float)data.IS_DIRTY_1,
                    (float)data.IND_SEQ_GPS_1,
                    (float)data.LATITUDE_1,
                    (float)data.LAT_COORD_1,
                    (float)data.LONGITUDE_1,
                    (float)data.LONG_COORD_1,
                    (float)data.GPS_QUAL_1,
                    (float)data.NUM_SAT_1,
                    (float)data.HDOP_1,
                    (float)data.ALTITUDE_1,
                    (float)data.GEO_SEPARATION_1,
                    (float)data.STN_1,
                    (float)data.AGE_1,
                    (float)data.UTC_1,
                    (float)data.DATA_GPS_1,
                    (float)data.TS_UNIX_GPS_1,
                    (float)data.MRU_ID,
                    (float)data.PORTA_COM_MRU,
                    (float)data.IND_SEQ_MRU,
                    (float)data.ROLL,
                    (float)data.YAW,
                    (float)data.PITCH,
                    (float)data.HEADING,
                    (float)data.SURGE,
                    (float)data.SWAY,
                    (float)data.HEAVE,
                    (float)data.VELSURGE,
                    (float)data.VELSWAY,
                    (float)data.VELHEAVE,
                    (float)data.ACESURGE,
                    (float)data.ACESWAY,
                    (float)data.ACEHEAVE,
                    (float)data.DATA_MRU,
                    (float)data.TS_UNIX_MRU
                }
            };
        }
    }
}
