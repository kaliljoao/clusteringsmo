﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace Clustering
{
    class Program
    {
        static readonly string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "C:\\Projects\\Clustering\\Clustering\\Data\\amostra.txt");
        static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "Data", "C:\\Projects\\data.zip");

        static void Main(string[] args)
        {
            var mlContext = new MLContext(seed: 0);
            List<TextLoader.Column> vectotLoader = new List<TextLoader.Column>();
            string[] Header = File.ReadAllLines(_dataPath);
            string[] splitHeader = Header[0].Split(',');
            int i = 0;
            foreach(string col in splitHeader)
            {
                TextLoader.Column novo = new TextLoader.Column(col, DataKind.Single, i);
                vectotLoader.Add(novo);
                i++;
            }

            Console.WriteLine("Clustering...");
            var loader = mlContext.Data.CreateTextLoader(vectotLoader.ToArray(), hasHeader: true, separatorChar: ',');
            IDataView dataView = loader.Load(_dataPath);
            string featuresColumnName = "Features";
            IEnumerable<ClusterData> enumerable = mlContext.Data.CreateEnumerable<ClusterData>(dataView, reuseRowObject: false);


            var pipeline = mlContext.Transforms
                .Concatenate(featuresColumnName, "LATITUDE", "LONGITUDE")
                .Append(mlContext.Clustering.Trainers.KMeans(featuresColumnName, clustersCount: 2));
            var model = pipeline.Fit(dataView);
            
            /*------ Evaluating the Model ------*/

            var predictions = model.Transform(dataView);
            var metrics = mlContext.Clustering.Evaluate(predictions);


            Console.WriteLine($"Average minimum score: {metrics.AvgMinScore}");
            var predictionFunc = model.CreatePredictionEngine<ClusterData, ClusterPrediction>(mlContext);
            using (var fileStream = new FileStream(_modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                mlContext.Model.Save(model, fileStream);
            }
            Console.WriteLine("End Process\n");

        }

    }
}



//Console.WriteLine($"Prediction - {prediction.PredictedClusterId}");

//using (var fileStream = new FileStream(_modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
//{
//    mlContext.Data.SaveAsText(data: dataView, fileStream,',');
//}

