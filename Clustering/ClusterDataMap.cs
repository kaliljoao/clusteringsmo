﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clustering
{
    public class ClusterDataMap : ClassMap<ClusterData>
    {
        public ClusterDataMap()
        {
            Initialize();
        }

        private void Initialize()
        {  
            Map(x => x.GIRO_ID);
            Map(x => x.PORTA_COM_GIRO);
            Map(x => x.IND_SEQ_GIRO);
            Map(x => x.ANGULO);
            Map(x => x.REFERENCIA);
            Map(x => x.DATA_GIRO);
            Map(x => x.TS_UNIX_GIRO);
            Map(x => x.GPS_ID);
            Map(x => x.PORTA_COM_GPS);
            Map(x => x.IS_CLEAN);
            Map(x => x.IS_DIRTY);
            Map(x => x.IND_SEQ_GPS);
            Map(x => x.LATITUDE);
            Map(x => x.LAT_COORD);
            Map(x => x.LONGITUDE);
            Map(x => x.LONG_COORD);
            Map(x => x.GPS_QUAL);
            Map(x => x.NUM_SAT);
            Map(x => x.HDOP);
            Map(x => x.ALTITUDE);
            Map(x => x.GEO_SEPARATION);
            Map(x => x.STN);
            Map(x => x.AGE);
            Map(x => x.UTC);
            Map(x => x.DATA_GPS);
            Map(x => x.TS_UNIX_GPS);
            Map(x => x.GPS_ID_1);
            Map(x => x.PORTA_COM_GPS_1);
            Map(x => x.IS_CLEAN_1);
            Map(x => x.IS_DIRTY_1);
            Map(x => x.IND_SEQ_GPS_1);
            Map(x => x.LATITUDE_1);
            Map(x => x.LAT_COORD_1);
            Map(x => x.LONGITUDE_1);
            Map(x => x.LONG_COORD_1);
            Map(x => x.GPS_QUAL_1);
            Map(x => x.NUM_SAT_1);
            Map(x => x.HDOP_1);
            Map(x => x.ALTITUDE);
            Map(x => x.GEO_SEPARATION_1);
            Map(x => x.STN_1);
            Map(x => x.AGE_1);
            Map(x => x.UTC_1);
            Map(x => x.DATA_GPS_1);
            Map(x => x.TS_UNIX_GPS_1);
            Map(x => x.MRU_ID);
            Map(x => x.PORTA_COM_MRU);
            Map(x => x.IND_SEQ_MRU);
            Map(x => x.ROLL);
            Map(x => x.YAW);
            Map(x => x.PITCH);
            Map(x => x.HEADING);
            Map(x => x.SURGE);
            Map(x => x.SWAY);
            Map(x => x.HEAVE);
            Map(x => x.VELSURGE);
            Map(x => x.VELSWAY);
            Map(x => x.VELHEAVE);
            Map(x => x.ACESURGE);
            Map(x => x.ACESWAY);
            Map(x => x.ACEHEAVE);
            Map(x => x.DATA_MRU);
            Map(x => x.TS_UNIX_MRU);
        }
    }
}
